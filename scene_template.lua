local storyboard = require( "storyboard" )
local scene = storyboard.newScene()
require "ship"
require "timebar"
 
local ship
local timebar
local progressIcon
local gameSpeed = 50
local winTime = 100
local currentTime = 0

function update(event)
    --make random shit happen here
    currentTime = currentTime + (gameSpeed / 100)
    timebar:update(currentTime / winTime)
    progressIcon.x = math.floor(timebar:getWidth() + 25)

    ship:update()

    
end
-- Called when the scene's view does not exist:
function scene:createScene( event )
    local group = self.view
    ship = Ship:new()
    progressIcon = display.newImage( "images/progress_icon.png" )
    progressIcon:setReferencePoint( display.TopLeftReferencePoint )
    progressIcon.x = 30
    progressIcon.y = 290
    timebar = Timebar:new()
    timebar.x = 32
    timebar.y = 300
    timebar:setReferencePoint( display.TopLeftReferencePoint )
    group:insert(timebar)
    group:insert(progressIcon)
    Runtime:addEventListener("enterFrame", update)
end
-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
    local group = self.view
end
-- Called when scene is about to move offscreen:
function scene:exitScene( event )
    local group = self.view
    group:remove(ship)
    Runtime:removeEventListener("enterFrame", update)
end
-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
    local group = self.view
end
scene:addEventListener( "createScene", scene )
scene:addEventListener( "enterScene", scene )
scene:addEventListener( "exitScene", scene )
scene:addEventListener( "destroyScene", scene )
 
return scene

