Man Overboard is supposed to be a pirate themed game where you work as a crew of pirates to defend your ship against the elements. Your ship will occasionally be struck by fire, or start to flood and it is up to you to save your ship. All of your ship has to survive by the team you reach land!

AUTHORS NOTE: An incredible amount of real life things got in the way of getting this game where I wanted it to be in 48 hours, so as a result it doesn't work. Currently you can guide a pirate to a ship tile, but because the drawing and the internal tiles are off, he can't correctly repair the tiles. I don't expect any points for a non-working game, but I wanted to submit it anyway. Thank you for your time and the opportunity to be apart of this experience!

-Justin Lindsey
Chronobit Studios