local STATE = {
    STRONG="STRONG",
    WEAK="WEAK",
    VERY_WEAK="VERY_WEAK",
    FIRE="FIRE",
    WATER="WATER",
    OCEAN="OCEAN",
}
return STATE
