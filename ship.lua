require "pirate"
require "tile"
Ship = {}

function Ship:new()
    local ship = display.newGroup()
    local minWidth = 1
    local minHeight = 0
    local shipWidth = 13
    local shipHeight = 8
    local tiles = {}
    local pirate = Pirate:new()

    for i = minWidth, shipWidth, 1 do
        tiles[i] = {}
        for j = minHeight, shipHeight, 1 do
            tiles[i][j] = Tile:new()
            tiles[i][j]:setReferencePoint( display.TopLeftReferencePoint )
            tiles[i][j]:setPos(i*32,j*32)
            --ship:insert(tiles[i][j])
            --tiles[i][j]:playAnimation(0,0,"weak")
        end
    end
    pirate:setReferencePoint( display.TopLeftReferencePoint )
    pirate.y = 50
    --ship:insert(pirate, 0)
    pirate:toFront()
    function ship:update()

        local eventChance = math.random(1,100)
        if eventChance == 100 then
            tiles[math.random(minWidth,shipWidth)][math.random(minHeight,shipHeight)]:playAnimation(0,0,"fire")
        end
        if eventChance == 2 then
            tiles[math.random(minWidth,shipWidth)][math.random(minHeight,shipHeight)]:playAnimation(0,0,"water")
        end

    end
    return ship
end
return Ship
