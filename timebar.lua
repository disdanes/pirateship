local display = require "display"

Timebar = {}

function Timebar:new()
    local timebar = display.newGroup()
    local chargingBar = display.newImage( "images/pixel_timebar.png" )
    local fullBar = display.newImage( "images/pixel_timebar_full.png" )
    local maxBarScale = 10
    chargingBar:setReferencePoint( display.TopLeftReferencePoint )
    fullBar:setReferencePoint( display.TopLeftReferencePoint )
    timebar:insert(fullBar)
    timebar:insert(chargingBar)
    chargingBar.xScale = 0
    fullBar.xScale = 0

    function timebar:update(percentIncrease)
        local newWidth = percentIncrease
        chargingBar.xScale, fullBar.xScale = newWidth, newWidth
        if timebar.xScale >= maxBarScale then
           timebar:setFull()
        else
           timebar:setCharging()
        end

    end

    function timebar:getWidth()
        return timebar.width
    end

    function timebar:setFull()
        fullBar:toFront()
    end

    function timebar:setCharging()
        chargingBar:toFront()
    end

    return timebar
end
return Timebar
