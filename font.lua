font = {}
--I made these as functions incase I need to do any dynamic fonts
function font:getFont()
    return "PF Tempesta Seven"
end
function font:getButtonFontSize()
    return 12
end
function font:getTitleSize()
    return 24
end
function font:getDamageSize()
    return 14
end
function font:getBattleSize()
    return 8
end

--TODO: Add support for font effects ie. fades for in combat

return font
