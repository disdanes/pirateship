-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------
-- Performance meter
local performance = require('performance')
performance:newPerformanceMeter()
--Initialize Math
math.randomseed( os.time() )
math.random()
math.random()

-- hide the status bar
display.setStatusBar( display.HiddenStatusBar )

-- include the Corona "storyboard" module
local storyboard = require "storyboard"

storyboard.purgeOnSceneChange = true
-- load menu screen
storyboard.gotoScene( "menu" )
