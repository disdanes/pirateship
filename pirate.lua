
Pirate = {}

function Pirate:new()
    local pirate = display.newGroup()
    local health = 32
    local sheetData = { width=32, height=32, numFrames=28, sheetContentWidth=128, sheetContentHeight=224 }
    local spriteSheet = graphics.newImageSheet("images/pirate.png", sheetData)
    local sequenceData = {
        { name="wait", frames={1,2,3,4}, time=1000, loopCount=0},
        { name="fixLeft", frames={5,6,7,8}, time=1000, loopCount=0},
        { name="fixRight", frames={9,10,11,12}, time=1000, loopCount=0},
        { name="walkLeft", frames={13,14,15,16}, time=1000, loopCount=0},
        { name="walkRight", frames={17,18,19,20}, time=1000, loopCount=0},
        { name="walkUp", frames={21,22,23,24}, time=1000, loopCount=0},
        { name="walkDown", frames={25,26,27,28}, time=1000, loopCount=0},
    }
    local animation = display.newSprite( spriteSheet, sequenceData )
    pirate:insert(animation)
    function pirate:playAnimation(x, y, animation_name)
        animation.x = x
        animation.y = y
        animation:setSequence( animation_name )
        animation:play()
    end
    function pirate:setPos(x,y)
        pirate.x = x
        pirate.y = y
    end

    function pirate:update()
    
    end

    function pirate:touch(event)
        if event.phase == "began" then
            markX = pirate.x
            markY = pirate.y
            display.getCurrentStage():setFocus( event.target )
            event.target.isFocus = true
        elseif event.target.isFocus then
            if event.phase == "moved" then
                local x = (event.x - event.xStart) + markX
                local y = (event.y - event.yStart) + markY
                pirate.x, pirate.y = x,y
            elseif event.phase == "ended" or event.phase == "cancelled" then
                pirate:playAnimation(0,0,"fixLeft")
                display.getCurrentStage():setFocus( nil )
                event.target.isFocus = false
            end
        end

        return true
    end
    pirate:addEventListener( "touch", fighter )
    return pirate
end
return Pirate
