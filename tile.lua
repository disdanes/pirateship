Tile = {}

function Tile:new()
    local tile = display.newGroup()
    local health = 32
    local sheetData = { width=32, height=32, numFrames=7, sheetContentWidth=96, sheetContentHeight=96 }
    local spriteSheet = graphics.newImageSheet("images/shiptiles.png", sheetData)
    local sequenceData = {
        { name="strong", frames={1}, time=1000, loopCount=0},
        { name="weak", frames={2}, time=1000, loopCount=0},
        { name="very_weak", frames={3}, time=1000, loopCount=0},
        { name="fire", frames={4,5}, time=1000, loopCount=0},
        { name="water", frames={6}, time=1000, loopCount=0},
        { name="ocean", frames={7}, time=1000, loopCount=0},
    }
    local animation = display.newSprite( spriteSheet, sequenceData )
    tile:insert(animation)
    function tile:playAnimation(x, y, animation_name)
        animation.x = x
        animation.y = y
        animation:setSequence( animation_name )
        animation:play()
    end
    function tile:setPos(x,y)
        tile.x = x
        tile.y = y
    end

    function tile:update()
    
    end

    return tile
end
return Tile
